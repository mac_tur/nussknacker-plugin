<?php

/**
 * Plugin Name:       NussKnacker
 * Plugin URI:        superskrypt.pl
 * Description:       Backend serwisu NussKnacker
 * Version:           $VERSION$
 * Author:            Superskrypt
 * Author URI:        superskrypt.pl
 */

if ( ! defined( 'WPINC' ) ) {
	die;
}

require_once __DIR__ . '/vendor/autoload.php';

\Superskrypt\WpBackendBase\WpBackendBase::setupBackend( 
    array( 
        'disable_posts' => false,
        'menu_pages_to_top' => true,
        'remove-embeds' => false,
        'themeSetup' => array(
            'add_theme_support' => array(
                'html5' => array('comment-list', 'comment-form', 'search-form', 'gallery', 'caption', 'style', 'script'),
                'responsive-embeds',
                'wp-block-styles',
                'align-wide',
                'editor-styles',
                'style-editor.css',
                'post-thumbnails' => array('post', 'page')
            )
        )
  
    ) 
);

require "ThemeOptions.php";

add_action( 'after_setup_theme', array("\Carbon_Fields\Carbon_Fields", "boot"));

\NussKnacker\ThemeOptions::register();

